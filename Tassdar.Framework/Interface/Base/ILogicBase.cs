﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tassdar.Framework.Model.Base;

namespace Tassdar.Framework.Interface.Base
{
    public  interface ILogicBase<TEntityBase>:ISlimLogicBase<TEntityBase>
        where TEntityBase:EntityBase,new()
    {
        IQueryable<TEntityBase> GetSortedQueryable();
    }
}
