﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using Tassdar.Framework.Interface.Base;
using Tassdar.Framework.Model.Base;

namespace Tassdar.Framework.WebApi
{
    public class ApiControllerBase<TEntity> : AbstractApiController
         where TEntity : EntityBase, new()
    {
        /// <summary>
        /// 初始化时请赋值
        /// </summary>
        protected ILogicBase<TEntity> _baseLogic;


        [HttpGet("base/list")]
        public virtual IList<TEntity> GetList(int pageIndex = 1, int pageSize = 20)
        {
            var data = _baseLogic.GetEntities(pageSize, pageIndex, out int rowsCount);
            return data;
        }

        /// <summary>
        /// 编辑保存
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost("base/edit")]
        public virtual ResultModel Edit([FromBody] TEntity form)
        {
            var res = _baseLogic.EditEntity(form);
            return res;
        }
        /// <summary>
        /// 添加保存
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost("base/add")]
        public virtual ResultModel Add([FromBody]TEntity form)
        {
            var res = _baseLogic.AddEntity(form);
            return res;
        }

        /// <summary>
        /// 添加集合
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        [HttpPut("base/add/list")]

        public virtual ResultModel AddList([FromBody] IList<TEntity> from)
        {
            var res = _baseLogic.AddEntities(from);
            return res;
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("base/delete")]
        public virtual ResultModel Remove(int id)
        {
            var res = _baseLogic.RemoveEntity(id);
            return res;
        }

    }
}
