﻿using System;
using System.Collections.Generic;
using System.Text;
using Tassdar.Framework.Interface.Base;
using Tassdar.Service.Entity;
using Tassdar.Service.Interface.Api;

namespace Tassdar.Service.Interface
{
    public interface IUser:ILogicBase<UserEnitty>, IUserApi
    {
    }
}
