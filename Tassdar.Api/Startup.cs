﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using log4net.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Tassdar.Core.Helper;
using Tassdar.Framework.Interface.Base;
using Tassdar.Framework.Model.Base;
using Tassdar.Logger;

namespace Tassdar.Api
{
    public class Startup
    {
        public static ILoggerRepository repository { get; set; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ConfigHelper.Configs = configuration;
            //创建仓储名字随意
            repository = LogManager.CreateRepository("NetCoreRepository");
            //加载log4配置文件
            XmlConfigurator.Configure(repository, new FileInfo("log4net.config"));
            
            InitRepository.loggerRepository = repository;
        }

        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            //添加过滤器
            services.AddMvc(o => o.Filters.Add(typeof(GlobalExceptions))).SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver());
            //注入EF上下文，注意生命周期是Transient
            services.AddDbContext<EFDbcontext>(x=>x.UseMySql(ConfigHelper.GetValue<string>("ConnectionsStrings:Development")),ServiceLifetime.Transient);
            //注入Swagger
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Version = "v1",
                    Title = "Tassdar"
                });
                options.ResolveConflictingActions(x => x.First());
                var xmlPath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "Tassdar.Api.xml");
                options.IncludeXmlComments(xmlPath);
            });
            //自动注入所有接口和实现类
            var totalAssembly = new[]
            {
                Assembly.Load("Tassdar.Service"),
            };
            services.Register(totalAssembly);
            DIHelper.ServiceProvider = services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(x =>
            {
                x.SwaggerEndpoint("/swagger/v1/swagger.json", "Tassdar API");
            });
        }
    }
}
