﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tassdar.Framework.Model.Base;
using Tassdar.Framework.WebApi;
using Tassdar.Service.Entity;
using Tassdar.Service.Interface;
using Tassdar.Service.Interface.Api;
using Tassdar.Service.Request;

namespace Tassdar.Api.Controllers
{
    /// <summary>
    /// 用户演示控制器
    /// </summary>
    [Route("api/user")]

    public class UserController : ApiControllerBase<UserEnitty>, IUserApi
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IUser _IUser;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IUser"></param>
        public UserController(IUser IUser)
        {
            _IUser = IUser;
            _baseLogic = IUser;
        }

        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        [HttpPost]
        public ResultModel AddUser([FromBody]UserRequest entity)
        {           
            return _IUser.AddUser(entity);
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("list")]
        public IList<UserEnitty> GetUserList()
        {
            return _IUser.GetUserList();
        }

        /// <summary>
        /// 分页列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        /// 
        [HttpGet("pager")]
        public (IList<UserEnitty>, int rows_count) GetUserList(int pageIndex, int pageSize)
        {
            return _IUser.GetUserList(pageIndex, pageSize);
        }

        /// <summary>
        /// 更改用户
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        /// 
        [HttpPut]
        public ResultModel UpdateUser(UserRequest entity)
        {
            return _IUser.UpdateUser(entity);
        }
    }
}